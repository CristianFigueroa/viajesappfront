import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListarCamionComponent } from './components/camion/listar-camion/listar-camion.component';
import { ListarConductorComponent } from './components/conductor/listar-conductor/listar-conductor.component';
import { IndexComponent } from './components/index/index/index.component';
import { ListarViajeComponent } from './components/viaje/listar-viaje/listar-viaje.component';

const routes: Routes = [
  { path: 'camiones', component: ListarCamionComponent},
  { path: 'viajes', component: ListarViajeComponent},
  { path: 'conductores', component: ListarConductorComponent},


  { path: 'index', component: IndexComponent },
  { path: '**', redirectTo: 'index', pathMatch: 'full' } // ruta erronea
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { relativeLinkResolution: 'legacy' })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
