import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { IndexComponent } from './components/index/index/index.component';
import { MenuComponent } from './components/menu/menu/menu.component';
import { NuevoCamionComponent } from './components/camion/nuevo-camion/nuevo-camion.component';
import { DetalleCamionComponent } from './components/camion/detalle-camion/detalle-camion.component';
import { ListarCamionComponent } from './components/camion/listar-camion/listar-camion.component';
import { EditarCamionComponent } from './components/camion/editar-camion/editar-camion.component';
import { EditarConductorComponent } from './components/conductor/editar-conductor/editar-conductor.component';
import { NuevoConductorComponent } from './components/conductor/nuevo-conductor/nuevo-conductor.component';
import { DetalleConductorComponent } from './components/conductor/detalle-conductor/detalle-conductor.component';
import { ListarConductorComponent } from './components/conductor/listar-conductor/listar-conductor.component';
import { ListarViajeComponent } from './components/viaje/listar-viaje/listar-viaje.component';
import { EditarViajeComponent } from './components/viaje/editar-viaje/editar-viaje.component';
import { NuevoViajeComponent } from './components/viaje/nuevo-viaje/nuevo-viaje.component';
import { DetalleViajeComponent } from './components/viaje/detalle-viaje/detalle-viaje.component';

@NgModule({
  declarations: [
    AppComponent,
    IndexComponent,
    MenuComponent,
    NuevoCamionComponent,
    DetalleCamionComponent,
    ListarCamionComponent,
    EditarCamionComponent,
    EditarConductorComponent,
    NuevoConductorComponent,
    DetalleConductorComponent,
    ListarConductorComponent,
    ListarViajeComponent,
    EditarViajeComponent,
    NuevoViajeComponent,
    DetalleViajeComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
